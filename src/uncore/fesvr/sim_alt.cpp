// Top-level driver for "verilated" objects (Verilog compiled with verilator)

// IITM Licence

// Imported from Rocket - fesvr integration
#include "verilated.h"
#include <memory>
#include "verilated_vcd_c.h"
// For option parsing, which is split across this file, Verilog, and
// FESVR's HTIF, a few external files must be pulled in. The list of
// files and what they provide is enumerated:
//
// DTM is a derived class on HTIF , Termiology used interchangably.
//
// $RISCV/include/fesvr/htif.h:
//   defines:
//     - HTIF_USAGE_OPTIONS
//     - HTIF_LONG_OPTIONS_OPTIND
//     - HTIF_LONG_OPTIONS
// $(ROCKETCHIP_DIR)/generated-src(-debug)?/$(CONFIG).plusArgs: only necessary if runtime plus args are used
//   defines:
//     - PLUSARG_USAGE_OPTIONS
//   variables:
//     - static const char * verilog_plusargs

#include <fesvr/dtm.h>

// General Purpose
#include "remote_bitbang.h"
#include <iostream>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

// Shakti Specific 
#include <cstdio>
#include "VmkTbSoc.h"

//- Rocket Imports
extern dtm_t* dtm;
extern remote_bitbang_t * jtag = NULL;

static uint64_t main_time  = 0;
bool verbose;
bool done_RST_N;

void handle_sigterm(int sig)
{
	dtm->stop();
}

// extern "C" int vpi_get_vlog_info(void* arg)
// {
// 	return 0;
// } //  VPI DPI compatibility ?


//vluint64_t main_time = 0;    // Current simulation time

double sc_time_stamp () {    // Called by $time in Verilog
		return main_time;
}

static void usage(const char * program_name){
	printf("Usage: %s [EMULATOR OPTION]... [VERILOG PLUSARG]... [HOST OPTION]... BINARY [TARGET OPTION]...\n",
				 program_name);
	fputs("\
Run a BINARY on the Rocket Chip emulator.\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
\n\
EMULATOR OPTIONS\n\
	-c, --cycle-count        Print the cycle count before exiting\n\
			 +cycle-count\n\
	-h, --help               Display this help and exit\n\
	-m, --max-cycles=CYCLES  Kill the emulation after CYCLES\n\
			 +max-cycles=CYCLES\n\
	-s, --seed=SEED          Use random number seed SEED\n\
	-r, --rbb-port=PORT      Use PORT for remote bit bang (with OpenOCD and GDB) \n\
													 If not specified, a random port will be chosen\n\
													 automatically.\n\
	-V, --verbose            Enable all Chisel printfs (cycle-by-cycle info)\n\
			 +verbose\n\
", stdout);
	fputs("\
	-v, --vcd=FILE,          Write vcd trace to FILE (or '-' for stdout)\n\
	-x, --dump-start=CYCLE   Start VCD tracing at CYCLE\n\
			 +dump-start\n\
", stdout);
	//fputs("\n", PLUSARG_USAGE_OPTIONS, stdout); // Not being pointed to right now ! Configure 
	printf("%s\n", HTIF_USAGE_OPTIONS);//, stdout);
	printf("\n"
"EXAMPLES\n"
"  - run a bare metal test:\n"
"    %s $RISCV/riscv64-unknown-elf/share/riscv-tests/isa/rv64ui-p-add\n"
"  - run a bare metal test showing cycle-by-cycle information:\n"
"    %s +verbose $RISCV/riscv64-unknown-elf/share/riscv-tests/isa/rv64ui-p-add 2>&1 | spike-dasm\n"
"  - run a bare metal test to generate a VCD waveform:\n"
"    %s -v rv64ui-p-add.vcd $RISCV/riscv64-unknown-elf/share/riscv-tests/isa/rv64ui-p-add\n"
"  - run an ELF (you wrote, called 'hello') using the proxy kernel:\n"
"    %s pk hello\n",
				 program_name, program_name, program_name,program_name );
}



// Main


int main(int argc, char** argv)
{
	printf("Starting Verilated C-Class With FESvr\n");
	unsigned random_seed = (unsigned)time(NULL) ^ (unsigned)getpid();
	uint64_t max_cycles = -1;
	int ret = 0;
	bool print_cycles = false;
	// Port numbers are 16 bit unsigned integers. 
	uint16_t rbb_port = 0;
	FILE * vcdfile = NULL;
	uint64_t start = 0;
	char ** htif_argv = NULL;
	int verilog_plusargs_legal = 1;

	while (1) {
		static struct option long_options[] = {
			{"cycle-count", no_argument,       0, 'c' },
			{"help",        no_argument,       0, 'h' },
			{"max-cycles",  required_argument, 0, 'm' },
			{"seed",        required_argument, 0, 's' },
			{"rbb-port",    required_argument, 0, 'r' },
			{"verbose",     no_argument,       0, 'V' },
			{"dump-start",  required_argument, 0, 'x' },
			HTIF_LONG_OPTIONS
		};
		int option_index = 0;
		int c = getopt_long(argc, argv, "-chm:s:r:v:Vx:", long_options, &option_index);
		if (c == -1) break;
 retry: // < This was written by someone in berkely or at SI - Five  Wow !
		switch (c) {
			// Process long and short EMULATOR options
			case '?': usage(argv[0]);             return 1;
			case 'c': print_cycles = true;        break;
			case 'h': usage(argv[0]);             return 0;
			case 'm': max_cycles = atoll(optarg); break;
			case 's': random_seed = atoi(optarg); break;
			case 'r': rbb_port = atoi(optarg);    break;
			case 'V': verbose = true;             break;
			case 'x': start = atoll(optarg);      break;
			// Process legacy '+' EMULATOR arguments by replacing them with
			// their getopt equivalents
			case 1: {
				std::string arg = optarg;
				if (arg.substr(0, 1) != "+") {
					optind--;
					goto done_processing;
				}
				if (arg == "+verbose")
					c = 'V';
				else if (arg.substr(0, 12) == "+max-cycles=") {
					c = 'm';
					optarg = optarg+12;
				}
				else if (arg.substr(0, 12) == "+dump-start=") {
					c = 'x';
					optarg = optarg+12;
				}
				else if (arg.substr(0, 12) == "+cycle-count")
					c = 'c';
				// If we don't find a legacy '+' EMULATOR argument, it still could be
				// a VERILOG_PLUSARG and not an error.
				else if (verilog_plusargs_legal) {
					const char ** plusarg = NULL;//&verilog_plusargs[0];
					int legal_verilog_plusarg = 0;
					while (*plusarg && (legal_verilog_plusarg == 0)){
						if (arg.substr(1, strlen(*plusarg)) == *plusarg) {
							legal_verilog_plusarg = 1;
						}
						plusarg ++;
					}
					if (!legal_verilog_plusarg) {
						verilog_plusargs_legal = 0;
					} else {
						c = 'P';
					}
					goto retry; 
				}
				// If we STILL don't find a legacy '+' argument, it still could be
				// an HTIF (HOST) argument and not an error. If this is the case, then
				// we're done processing EMULATOR and VERILOG arguments.
				else {
					static struct option htif_long_options [] = { HTIF_LONG_OPTIONS };
					struct option * htif_option = &htif_long_options[0];
					while (htif_option->name) {
						if (arg.substr(1, strlen(htif_option->name)) == htif_option->name) {
							optind--;
							goto done_processing;
						}
						htif_option++;
					}
					std::cerr << argv[0] << ": invalid plus-arg (Verilog or HTIF) \""
										<< arg << "\"\n";
					c = '?';
				}
				goto retry;
			}
			case 'P': break; // Nothing to do here, Verilog PlusArg
			// Realize that we've hit HTIF (HOST) arguments or error out
			default:
				if (c >= HTIF_LONG_OPTIONS_OPTIND) {
					optind--;
					goto done_processing;
				}
				c = '?';
				goto retry;
		}
	}

done_processing:
	if (optind == argc) {
		std::cerr << "No binary specified for emulator\n";
		usage(argv[0]);
		return 1;
	}
	int htif_argc = 1 + argc - optind;
	htif_argv = (char **) malloc((htif_argc) * sizeof (char *));
	htif_argv[0] = argv[0];
	for (int i = 1; optind < argc;) htif_argv[i++] = argv[optind++];

	if (verbose)
		fprintf(stderr, "using random seed %u\n", random_seed);

	srand(random_seed);
	srand48(random_seed);

	// Verilated::randRST_N(2);
	// Verilated::commandArgs(argc, argv);
	// TEST_HARNESS *mkTbSoc = new TEST_HARNESS;
	
	jtag = new remote_bitbang_t(rbb_port);
	dtm = new dtm_t(htif_argc, htif_argv);

	signal(SIGTERM, handle_sigterm);

// -- 
	VmkTbSoc* mkTbSoc = new VmkTbSoc;    // create instance of model

    mkTbSoc->RST_N = 0;    // assert RST_N
    mkTbSoc->CLK = 0;

	Verilated::traceEverOn(true);
    VerilatedVcdC* tfp = new VerilatedVcdC;
    mkTbSoc->trace (tfp, 99);
    tfp->open ("shakti.vcd");

	
	bool dump;
	// RST_N for several cycles to handle pipelined RST_N
	for (int i = 0; i < 10; i++) {
		mkTbSoc->RST_N = 1;
		mkTbSoc->CLK = 0;
		mkTbSoc->eval();
		tfp->dump (main_time);
		main_time++;
		mkTbSoc->CLK = 1;
		mkTbSoc->eval();
		tfp->dump (main_time);
		main_time++;
	}

	mkTbSoc->RST_N = 0;
	done_RST_N = true;

	//while (!dtm->done() && !jtag->done() && main_time  < max_cycles) {
	while (! Verilated::gotFinish ()) {
		mkTbSoc->CLK = 0;
		mkTbSoc->eval();
		tfp->dump (main_time);
		main_time++;
		mkTbSoc->CLK = 1;
		mkTbSoc->eval();
	    tfp->dump (main_time);
		main_time++;
	}


	if (dtm->exit_code())
	{
		fprintf(stderr, "*** FAILED *** via dtm (code = %d, seed %d) after %ld cycles\n", dtm->exit_code(), random_seed, main_time );
		ret = dtm->exit_code();
	}
	else if (jtag->exit_code())
	{
		fprintf(stderr, "*** FAILED *** via jtag (code = %d, seed %d) after %ld cycles\n", jtag->exit_code(), random_seed, main_time );
		ret = jtag->exit_code();
	}
	else if (main_time  == max_cycles)
	{
		fprintf(stderr, "*** FAILED *** via main_time  (timeout, seed %d) after %ld cycles\n", random_seed, main_time );
		ret = 2;
	}
	else if (verbose || print_cycles)
	{
		fprintf(stderr, "*** PASSED *** Completed after %ld cycles\n", main_time );
	}

	if (dtm) delete dtm;
	if (jtag) delete jtag;
	if (htif_argv) free(htif_argv);

    tfp->close();

	mkTbSoc->final ();    // Done simulating
	delete mkTbSoc;
	mkTbSoc = NULL;
	exit (0);
	}
