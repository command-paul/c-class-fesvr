// See LICENSE.SiFive for license details.

#include <fesvr/dtm.h>
#include "vpi_user.h"
//#include "svdpi.h"
#include <cstdio>
#include <bitset>

dtm_t* dtm;

typedef struct resp_feild{
  unsigned int req_valid 		: 1 ;
  unsigned int req_bits_addr 	: 6 ;
  unsigned int req_bits_op 		: 2 ;
  unsigned int req_bits_data 	:32 ;
  unsigned int resp_ready 		: 1 ;
}resp_feild;

//   unsigned int* resultptr,
//   unsigned char  debug_req_ready,
//   unsigned char  debug_resp_valid,
//   int            debug_resp_bits_resp,
//   int            debug_resp_bits_data

extern "C" unsigned long long int sim_fesvr_request(unsigned long long int input_frame)
{
  unsigned long long int result;
  resp_feild resp_vector;
  unsigned int debug_req_ready  = (input_frame & 0x00000000ffffffff)  >> 0; 
  unsigned int debug_resp_valid = (input_frame & 0x0000000300000000)  >> 32; 
  unsigned int debug_resp_bits_resp = (input_frame & 0x0000000400000000)  >> 34; 
  unsigned int debug_resp_bits_data = (input_frame & 0x0000000800000000)  >> 35;
  printf("Sim DTM 1\n");

  if (!dtm) {
    s_vpi_vlog_info info;
    if (!vpi_get_vlog_info(&info))
      abort();
    dtm = new dtm_t(info.argc, info.argv);
  }
  printf("Sim DTM 2\n");

  dtm_t::resp resp_bits;
  resp_bits.resp = debug_resp_bits_resp;
  resp_bits.data = debug_resp_bits_data; 
  
  dtm->tick
  (
    debug_req_ready,
    debug_resp_valid,
    resp_bits
  );
   printf("Sim DTM 3\n");

  resp_vector.req_valid 	= dtm->req_valid(); 	//debug_req_valid
  resp_vector.req_bits_addr = dtm->req_bits().addr;	//debug_req_bits_addr
  resp_vector.req_bits_op 	= dtm->req_bits().op;   //debug_req_bits_op
  resp_vector.req_bits_data = dtm->req_bits().data; //debug_req_bits_data
  resp_vector.resp_ready 	= dtm->resp_ready();  	//debug_resp_ready
  memcpy(&result,&resp_vector,sizeof(resp_vector));
  return result;
}
