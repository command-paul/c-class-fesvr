// Lesson :: If everything is looking good you're doing something terribly wrong !

package Fesvr;
	import SimDTM::*;

	interface Ifc_Fesvr_DTM;
		method ActionValue#(Bit#(40)) pollRequest(); // ! Has Conditions
		method Action serviceResponse(Bit#(34) frame);	// ! Has Conditions
	endinterface

	/*
	->>> RISC-V fesvr Adapter FSM

	Idle ( if request valid ) - >  SubmitRequest
	SubmitRequest -(on calling PollRequest)> ProcessRequest
	ProcessRequest ( if response ready) - > SubmitResponse
	SubmitResponse -(on Calling ServiceResponse)> TxComplete
	TxComplete -> Idle // Auto Transition 	
	
	// Reset Condition if request valid or response ready de assert early then reset ! 

	->>> No Rule for Reset This FSM Can fall into a Dead Lock with a non responsive fesvr host !
	*/
	typedef enum {Idle,SubmitRequest,ProcessRequest,SubmitResponse,TxComplete} StateType deriving (Eq, Bits);

	module mkFesvrDTM(Ifc_Fesvr_DTM);
		// State Register
		Reg#(StateType) curr_state <- mkReg (Idle);
		
		Reg#(FesvrMessage) 	fesvrInput 	<- mkReg(unpack(0));
		Reg#(FesvrResponse) dtm_response<- mkReg(unpack(0));

		// IDLE
		(* fire_when_enabled, no_implicit_conditions *)
		rule state_Idle (curr_state == Idle);
			let d_resp_resp = dtm_response.debug_resp_bits_resp;
			let d_resp_data = dtm_response.debug_resp_bits_data;
			if(fesvrInput.fesvr_req_valid == 1'b1) begin
				curr_state <= SubmitRequest;
				dtm_response <= FesvrResponse{debug_req_ready:1'b0,debug_resp_valid:1'b0,debug_resp_bits_resp:d_resp_resp,debug_resp_bits_data:d_resp_data};
				end
			else begin
				let x <- sim_fesvr_request(dtm_response);
				dtm_response <= FesvrResponse{debug_req_ready:1'b1,debug_resp_valid:1'b0,debug_resp_bits_resp:d_resp_resp,debug_resp_bits_data:d_resp_data};
				fesvrInput <= x;		// No Implict conditions asserrted This BDPI method should not have any conditions 
				end
		endrule

// Process Request
		(* fire_when_enabled, no_implicit_conditions *)
		rule state_ProcessRequest (curr_state == ProcessRequest);
			if(fesvrInput.fesvr_resp_ready == 1'b1)
				curr_state <= SubmitResponse;
			else begin 
				let x <- sim_fesvr_request(dtm_response);
				fesvrInput <= x;		// No Implict conditions asserrted This BDPI method should not have any conditions 
				end
		endrule

// Tx_Complete
		(* fire_when_enabled ,no_implicit_conditions *)
		rule state_TxComplete (curr_state == TxComplete);
			dtm_response.debug_resp_valid <= 1'b1;
			let x <- sim_fesvr_request(dtm_response);
			fesvrInput <= x;		// No Implict conditions asserrted This BDPI method should not have any conditions 
		endrule

		// Interface Ifc_Fesvr_DTM Methods
		method ActionValue#(Bit#(40)) pollRequest()	if (curr_state == SubmitRequest)	;
			// Return Request Data
			let x = {fesvrInput.fesvr_req_op,fesvrInput.fesvr_req_addr,fesvrInput.fesvr_req_data}; //check order
			curr_state <= ProcessRequest;
			return unpack(x);
			//return 40'h0000000000;
		endmethod

		method Action serviceResponse(Bit#(34)frame)		if (curr_state == SubmitResponse)	;
			let d_rq_rdy = dtm_response.debug_req_ready;
			let d_resp_valid = dtm_response.debug_resp_valid;
			dtm_response <= FesvrResponse{debug_req_ready:d_rq_rdy,debug_resp_valid:d_resp_valid,debug_resp_bits_resp:frame[1:0],debug_resp_bits_data:frame[33:2]};
			//			dtm_response.debug_resp_bits_resp <= frame[1:0];
			//			dtm_response.debug_resp_bits_data <= frame[33:2];
			//collect request data
			curr_state <= TxComplete;
		endmethod

	endmodule
endpackage



///// Abandoned Attempt at making a Very Functional FSM 
///// Abandoned because Bluespec does not recommend Functional FSM`s when
/////// "Actions" are performed inside a function 
/////// It recommends the multi rule approach to writing FSMS in that case 
//
//// Abandoned the below because I made a mistake of not properly modelling the test bench side interface 
//
