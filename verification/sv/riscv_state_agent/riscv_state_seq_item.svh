//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//


// -----------------------------------------------------------------------------------------------
// riscv_state_seq_item
// -----------------------------------------------------------------------------------------------
class riscv_state_seq_item extends uvm_sequence_item;

	// uvm factory registation
	`uvm_object_utils_begin(riscv_state_seq_item)
  `uvm_object_utils_end
	
  // data members
  bit [`DATA_WIDTH-1:0] int_regs [$];
  bit [`DATA_WIDTH-1:0] fp_regs [$];
  //bit [`WORD_WIDTH-1:0] csr_index[] = {`CSR_CYCLE, `CSR_INSTRET, `CSR_STVEC, `CSR_SEPC, `CSR_SCAUSE ,  `CSR_SATP ,  `CSR_MSTATUS, `CSR_MEDELEG, `CSR_MIDELEG, `CSR_MIE, `CSR_MTVEC, `CSR_MEPC, `CSR_MCAUSE, `CSR_MTVAL, `CSR_MIP};
//  bit [`WORD_WIDTH-1:0] csr_index[] = {`CSR_CYCLE , `CSR_TIME , `CSR_INSTRET , `CSR_SSTATUS , `CSR_SIE , `CSR_STVEC , `CSR_SCOUNTEREN , `CSR_SSCRATCH , `CSR_SEPC , `CSR_SCAUSE , `CSR_STVAL , `CSR_SIP , `CSR_SATP , `CSR_MSTATUS , `CSR_MISA , `CSR_MEDELEG , `CSR_MIDELEG , `CSR_MIE , `CSR_MTVEC , `CSR_MCOUNTEREN , `CSR_MSCRATCH , `CSR_MEPC , `CSR_MCAUSE , `CSR_MTVAL , `CSR_MIP , `CSR_TSELECT , `CSR_TDATA1 , `CSR_TDATA2 , `CSR_TDATA3 , `CSR_DCSR , `CSR_DPC , `CSR_DSCRATCH , `CSR_MCYCLE , `CSR_MINSTRET , `CSR_MVENDORID , `CSR_MARCHID , `CSR_MIMPID , `CSR_MHARTID , `CSR_CYCLEH , `CSR_TIMEH , `CSR_INSTRETH , `CSR_MCYCLEH , `CSR_MINSTRETH};
  
  bit [`WORD_WIDTH-1:0] csr_index[] = {`CSR_CYCLE , `CSR_INSTRET , `CSR_SSTATUS , `CSR_SIE , `CSR_STVEC , `CSR_SCOUNTEREN , `CSR_SSCRATCH , `CSR_SEPC , `CSR_SCAUSE , `CSR_STVAL , `CSR_SIP , `CSR_SATP , `CSR_MSTATUS , `CSR_MISA , `CSR_MEDELEG , `CSR_MIDELEG , `CSR_MIE , `CSR_MTVEC , `CSR_MCOUNTEREN , `CSR_MSCRATCH , `CSR_MEPC , `CSR_MCAUSE , `CSR_MTVAL , `CSR_MIP ,`CSR_MCYCLE , `CSR_MINSTRET};
  typedef bit [`WORD_WIDTH-1:0] csrType; 
  bit [`DATA_WIDTH-1:0] csr [csrType]; 
  bit [`DATA_WIDTH-1:0] mem_data;
  bit [`ADDR_WIDTH-1:0] addr;
  bit [`DATA_WIDTH-1:0] pc;

	// uvm methods
	extern function new(string name="riscv_state_seq_item", uvm_component parent=null);
  extern function void print_riscv_state();
  extern function string get_csr_name(input bit [31:0] index);
endclass : riscv_state_seq_item

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function riscv_state_seq_item::new(string name="riscv_state_seq_item", uvm_component parent=null);
  super.new(name);
endfunction : new


// -----------------------------------------------------------------------------------------------
// print_riscv_state
// -----------------------------------------------------------------------------------------------
function void riscv_state_seq_item::print_riscv_state();
  logic [31:0] temp;
  string csr_string = "";
  
  for(temp='h0; temp<csr_index.size; temp++) begin
    logic [31:0] index;
    string temp_string;
    index = csr_index[temp];
    $sformat(temp_string,"[%s]\t: %h\t", get_csr_name(index),csr[index]);
    if ((temp+1)%3==0) begin
      csr_string = {csr_string ,temp_string, "\n"};
    end 
    else begin
      csr_string = {csr_string ,temp_string};
    end
  end

  `uvm_info(get_full_name(), $sformatf("\n[pc] : 0x%h\n[zero|x0] : 0x%h\t  [ra|x1] : 0x%h\t  [sp |x2] : 0x%h\t  [gp |x3] : 0x%h\n[tp  |x4] : 0x%h\t  [t0|x5] : 0x%h\t  [t1 |x6] : 0x%h\t  [t2 |x7] : 0x%h\n[s0  |x8] : 0x%h\t  [s1|x9] : 0x%h\t  [a0 |x10]: 0x%h\t  [a1 |x11]: 0x%h\n[a2  |x12]: 0x%h\t  [a3|x13]: 0x%h\t  [a4 |x14]: 0x%h\t  [a5 |x15]: 0x%h\n[a6  |x16]: 0x%h\t  [a7|x17]: 0x%h\t  [s2 |x18]: 0x%h\t  [s3 |x19]: 0x%h\n[s4  |x20]: 0x%h\t  [s5|x21]: 0x%h\t  [s6 |x22]: 0x%h\t  [s7 |x23]: 0x%h\n[s8  |x24]: 0x%h\t  [s9|x25]: 0x%h\t  [s10|x26]: 0x%h\t  [s11|x27]: 0x%h\n[t3  |x28]: 0x%h\t  [t4|x29]: 0x%h\t  [t5 |x30]: 0x%h\t  [t6 |x31]: 0x%h\n%s",pc,
int_regs[0],int_regs[1],int_regs[2],int_regs[3],
int_regs[4],int_regs[5],int_regs[6],int_regs[7], 
int_regs[8],int_regs[9],int_regs[10],int_regs[11],  
int_regs[12],int_regs[13],int_regs[14],int_regs[15],
int_regs[16],int_regs[17],int_regs[18],int_regs[19],
int_regs[20],int_regs[21],int_regs[22],int_regs[23],
int_regs[24],int_regs[25],int_regs[26],int_regs[27],
int_regs[28],int_regs[29],int_regs[30],int_regs[31], csr_string), 
UVM_LOW)

endfunction : print_riscv_state

function string riscv_state_seq_item::get_csr_name(input bit [31:0] index);
      case (index)
        `CSR_CYCLE      : return "csr_cycle";
        `CSR_TIME       : return "csr_time";
        `CSR_INSTRET    : return "csr_instret";
        `CSR_SSTATUS    : return "csr_sstatus";
        `CSR_SIE        : return "csr_sie";
        `CSR_STVEC      : return "csr_stvec";
        `CSR_SCOUNTEREN : return "scounteren";
        `CSR_SSCRATCH   : return "csr_sscratch";
        `CSR_SEPC       : return "csr_sepc";
        `CSR_SCAUSE     : return "csr_scause";
        `CSR_STVAL      : return "csr_stval";
        `CSR_SIP        : return "csr_sip";
        `CSR_SATP       : return "csr_satp";
        `CSR_MSTATUS    : return "csr_mstatus";
        `CSR_MISA       : return "csr_misa";
        `CSR_MEDELEG    : return "csr_medeleg";
        `CSR_MIDELEG    : return "csr_mideleg";
        `CSR_MIE        : return "csr_mie";
        `CSR_MTVEC      : return "csr_mtvec";
        `CSR_MCOUNTEREN : return "mcounteren";
        `CSR_MSCRATCH   : return "csr_mscratch";
        `CSR_MEPC       : return "csr_mepc";
        `CSR_MCAUSE     : return "csr_mcause";
        `CSR_MTVAL      : return "csr_mtval";
        `CSR_MIP        : return "csr_mip";
        `CSR_TSELECT    : return "csr_tselect";
        `CSR_TDATA1     : return "csr_tdata1";
        `CSR_TDATA2     : return "csr_tdata2";
        `CSR_TDATA3     : return "csr_tdata3";
        `CSR_DCSR       : return "csr_dcsr";
        `CSR_DPC        : return "csr_dpc";
        `CSR_DSCRATCH   : return "csr_dscratch";
        `CSR_MCYCLE     : return "csr_mcycle";
        `CSR_MINSTRET   : return "csr_minstret";
        `CSR_MVENDORID  : return "csr_mvendorid";
        `CSR_MARCHID    : return "csr_marchid";
        `CSR_MIMPID     : return "csr_mimpid";
        `CSR_MHARTID    : return "csr_mhartid";
        `CSR_CYCLEH     : return "csr_cycleh";
        `CSR_TIMEH      : return "csr_timeh";
        `CSR_INSTRETH   : return "csr_instreth";
        `CSR_MCYCLEH    : return "csr_mcycleh";
        `CSR_MINSTRETH  : return "csr_minstreth";
      default:               $display("csr_INVALID");
    endcase
endfunction
