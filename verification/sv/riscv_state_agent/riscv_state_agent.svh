//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// riscv_state_agent
// -----------------------------------------------------------------------------------------------
class riscv_state_agent extends uvm_agent;

	// uvm factory registation
	`uvm_component_utils_begin(riscv_state_agent)
  `uvm_component_utils_end
	
  // data members
  riscv_state_agent_config state_cfg;

	// component members
  uvm_analysis_port #(riscv_state_seq_item) agent_state_port;
  uvm_analysis_port #(riscv_state_seq_item) agent_spike_port;
  riscv_state_monitor state_monitor;
  riscv_state_predictor state_predictor;

	// uvm methods
	extern function new(string name="riscv_state_agent", uvm_component parent);
	extern function void build_phase(uvm_phase phase);
	extern function void connect_phase(uvm_phase phase);
endclass : riscv_state_agent

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function riscv_state_agent::new(string name="riscv_state_agent", uvm_component parent);
	super.new(name, parent);
endfunction : new

// -----------------------------------------------------------------------------------------------
// build
// -----------------------------------------------------------------------------------------------
function void riscv_state_agent::build_phase(uvm_phase phase);
	//super.build(phase);
  if (!uvm_config_db #(riscv_state_agent_config)::get(this,"*","riscv_state_agent_config", state_cfg)) begin
     `uvm_fatal("FATAL MSG", "Configuration object is not set properly")
  end
  agent_state_port = new("agent_state_port", this);
  agent_spike_port = new("agent_spike_port", this);
  state_monitor = riscv_state_monitor::type_id::create("state_monitor", this);
  state_predictor = riscv_state_predictor::type_id::create("state_predictor", this);
endfunction : build_phase

// -----------------------------------------------------------------------------------------------
// connect
// -----------------------------------------------------------------------------------------------
function void riscv_state_agent::connect_phase(uvm_phase phase);
  state_monitor.riscv_state_port.connect(agent_state_port);
  state_predictor.spike_state_port.connect(agent_spike_port);
endfunction : connect_phase
