//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// riscv_state_agent_config
// -----------------------------------------------------------------------------------------------
class riscv_state_agent_config extends uvm_object;

	// uvm factory registation
	`uvm_object_utils_begin(riscv_state_agent_config)
  `uvm_object_utils_end
	// data members

  virtual riscv_state_if state_if;
	// component members
  uvm_active_passive_enum active = UVM_PASSIVE;

	// uvm methods
	extern function new(string name="riscv_state_agent_config", uvm_component parent=null);
endclass : riscv_state_agent_config

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function riscv_state_agent_config::new(string name="riscv_state_agent_config", uvm_component parent=null);
  super.new(name);
endfunction : new
