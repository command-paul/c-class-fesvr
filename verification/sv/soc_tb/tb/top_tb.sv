module top_tb;

//`include "timescale.v"

import uvm_pkg::*;
import riscv_state_agent_pkg::*;
import riscv_test_pkg::*;

reg clk;
wire rst_n;

// interface instantiation
riscv_state_if state_if(clk,rst_n);

// dut instantiation
mkTbSoc tb(.CLK(clk),.RST_N(rst_n));

initial begin
  clk=0;
  forever #10 clk = ~clk;
end
task do_run;
  forever @(posedge clk) begin
    if (state_if.instr_retire == 'b1) begin
      $display("pc=%h x5=%h  x10=%h x11=%h",state_if.pc, state_if.x5, state_if.x10,state_if.x11);
    end
    //$display("\n code=%h %h",tb.soc.main_memory_dmemMSB.RAM[istate_if.pc_value],tb.soc.main_memory_dmemLSB.RAM[pc]);
    //$display("\n boot=%h %h ",tb.soc.bootrom.dmemMSB.RAM[pc], tb.soc.bootrom.dmemLSB.RAM[pc]);
  end
endtask
initial begin
  //rst_n = 0;
  $readmemh("boot.LSB", tb.soc.bootrom.dmemLSB.RAM);
  $readmemh("boot.MSB", tb.soc.bootrom.dmemMSB.RAM);
  $readmemh("code.mem.LSB", tb.soc.main_memory_dmemLSB.RAM);
  $readmemh("code.mem.MSB", tb.soc.main_memory_dmemMSB.RAM);
  //rst_n = 1;
  uvm_config_db #(virtual riscv_state_if)::set(null, "*", "state_if", state_if);
//  #50;
//  rst_n = 1;
 // do_run();
  run_test();
  $finish;
end
initial begin
  $dumpfile("test.vcd");
  $dumpvars(0,tb);
  //$fsdbDumpvars;
end

assign state_if.pc =  `riscv.rx_w_data$wget[146:108];
assign state_if.instr_retire = `riscv.WILL_FIRE_RL_rl_write_back;

assign state_if.x0 =  `integer_rf.arr[0];
assign state_if.x1 =  `integer_rf.arr[1];
assign state_if.x2 =  `integer_rf.arr[2];
assign state_if.x3 =  `integer_rf.arr[3];
assign state_if.x4 =  `integer_rf.arr[4];
assign state_if.x5 =  `integer_rf.arr[5];
assign state_if.x6 =  `integer_rf.arr[6];
assign state_if.x7 =  `integer_rf.arr[7];
assign state_if.x8 =  `integer_rf.arr[8];
assign state_if.x9 =  `integer_rf.arr[9];
assign state_if.x10 = `integer_rf.arr[10];
assign state_if.x11 = `integer_rf.arr[11];
assign state_if.x12 = `integer_rf.arr[12];
assign state_if.x13 = `integer_rf.arr[13];
assign state_if.x14 = `integer_rf.arr[14];
assign state_if.x15 = `integer_rf.arr[15];
assign state_if.x16 = `integer_rf.arr[16];
assign state_if.x17 = `integer_rf.arr[17];
assign state_if.x18 = `integer_rf.arr[18];
assign state_if.x19 = `integer_rf.arr[19];
assign state_if.x20 = `integer_rf.arr[20];
assign state_if.x21 = `integer_rf.arr[21];
assign state_if.x22 = `integer_rf.arr[22];
assign state_if.x23 = `integer_rf.arr[23];
assign state_if.x24 = `integer_rf.arr[24];
assign state_if.x25 = `integer_rf.arr[25];
assign state_if.x26 = `integer_rf.arr[26];
assign state_if.x27 = `integer_rf.arr[27];
assign state_if.x28 = `integer_rf.arr[28];
assign state_if.x29 = `integer_rf.arr[29];
assign state_if.x30 = `integer_rf.arr[30];
assign state_if.x31 = `integer_rf.arr[31];
assign state_if.data_lsb = tb.soc.bootrom.dmemLSB.RAM[0];
assign state_if.data_msb = tb.soc.bootrom.dmemMSB.RAM[0];
assign rst_n = state_if.rst_n;

// to review the concatenation bit size with spec
// mstatus[16:15] => XS is 0 ?
// SD ?
 assign state_if.csr_cycle = `csr.csr_mcycle;
// assign state_if.csr_time;
 assign state_if.csr_instret = `csr.csr_minstret;
 assign state_if.csr_sstatus = {29'b0,`csr.rg_mxl,12'b0,`csr.rg_mxr,`csr.rg_sum,1'b0,2'b0,`csr.rg_fs, 4'b0,`csr.rg_spp,2'b0,`csr.rg_spie,`csr.rg_upie,2'b0,`csr.rg_sie,`csr.rg_uie};
assign state_if.csr_sie = {54'b0, `csr.rg_seie,`csr.rg_ueie, 2'b0,`csr.rg_stie,`csr.rg_utie, 2'b0,`csr.rg_ssie, `csr.rg_usie};
 assign state_if.csr_stvec = {`csr.rg_stvec, `csr.rg_mode_s};
 assign state_if.csr_scounteren = `csr.rg_scounteren;
 assign state_if.csr_sscratch = `csr.csr_sscratch;
 assign state_if.csr_sepc = `csr.csr_sepc;
 assign state_if.csr_scause = `csr.csr_scause;
 assign state_if.csr_stval = {25'b0,`csr.rg_stval};
assign state_if.csr_sip = {54'b0,(`csr.rg_seipe | `csr.rg_seips) ,(`csr.rg_ueipe | `csr.rg_ueips) , 2'b0,`csr.rg_stip,`csr.rg_utip,2'b0,`csr.rg_ssip,`csr.rg_usip};
 assign state_if.csr_satp = `csr.csr_satp;
 assign state_if.csr_mstatus = {27'b0,`csr.rg_mxl,`csr.rg_mxl,9'b0,`csr.rg_tsr,`csr.rg_tw,`csr.rg_tvm,`csr.rg_mxr,`csr.rg_sum,`csr.rg_mprv,2'b0,`csr.rg_fs,`csr.rg_mpp,2'b0,`csr.rg_spp,`csr.rg_mpie,1'b0,`csr.rg_spie,`csr.rg_upie,`csr.rg_mie,1'b0,`csr.rg_sie,`csr.rg_uie};
 assign state_if.csr_misa = {`csr.rg_mxl,36'b0, `csr.rg_misa};
assign state_if.csr_medeleg = {48'b0,`csr.rg_medeleg};
// mideleg follows mip layout: only 12 bits defined in spec.. extra 2 for
// debug halt and debug resume
assign state_if.csr_mideleg = {49'b0, `csr.rg_mideleg};
assign state_if.csr_mie = {52'b0, `csr.rg_meie,1'b0,`csr.rg_seie,`csr.rg_ueie,`csr.rg_mtie,1'b0,`csr.rg_stie,`csr.rg_utie,`csr.rg_msie,1'b0,`csr.rg_ssie, `csr.rg_usie};
assign state_if.csr_mtvec = {`csr.rg_mtvec, `csr.rg_mode_m};
 assign state_if.csr_mcounteren = `csr.reg_mcounteren;
 assign state_if.csr_mscratch = `csr.csr_mscratch;
assign state_if.csr_mepc = `csr.rg_mepc;
assign state_if.csr_mcause = {32'b0,`csr.rg_interrupt,`csr.rg_lower_cause};
assign state_if.csr_mtval = {25'b0,`csr.rg_mtval};
assign state_if.csr_mip = {52'b0,`csr.rg_meip,1'b0,(`csr.rg_seipe | `csr.rg_seips) ,(`csr.rg_ueipe | `csr.rg_ueips) , `csr.rg_mtip, 1'b0,`csr.rg_stip,`csr.rg_utip,`csr.rg_msip,1'b0,`csr.rg_ssip,`csr.rg_usip};
// assign state_if.csr_tselect;
// assign state_if.csr_tdata1;
// assign state_if.csr_tdata2;
// assign state_if.csr_tdata3;
// assign state_if.csr_dcsr;
// assign state_if.csr_dpc;
// assign state_if.csr_dscratch;
 assign state_if.csr_mcycle = `csr.csr_mcycle;
 assign state_if.csr_minstret = `csr.csr_minstret;
// assign state_if.csr_mvendorid;
// assign state_if.csr_marchid;
// assign state_if.csr_mimpid;
// assign state_if.csr_mhartid;
// assign state_if.csr_cycleh;
// assign state_if.csr_timeh;
// assign state_if.csr_instreth;
// assign state_if.csr_mcycleh;
// assign state_if.csr_minstreth;

//assign state_if.csr_mtime = ;
//assign state_if.csr_mtimecmp = clint_mtime_c_mtime;

endmodule
